package main

import (
	"context"
	"factory/internal/config"
	handlermanager "factory/internal/handlers/manager"
	"factory/pkg/client/postgresql"
	"factory/pkg/logging"
	repeatable "factory/pkg/utils"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/cors"
)

func main() {

	cfg := config.GetConfig()
	logger := logging.GetLogger()

	err := repeatable.CrateDir()
	if err != nil {
		logger.Fatalf("%v", err)
	}

	postgresSQLClient := startPostgresql(cfg, logger)

	start(handlermanager.Manager(postgresSQLClient, logger), cfg)
}

func startPostgresql(cfg *config.Config, logger *logging.Logger) *pgxpool.Pool {
	postgresSQLClient, err := postgresql.NewClient(context.TODO(), 3, cfg.Storage)
	if err != nil {
		logger.Error("FATAL  ERROR .......", err)
	}
	return postgresSQLClient
}

func start(router *mux.Router, cfg *config.Config) {
	logger := logging.GetLogger()
	logger.Info("start application")

	var listener net.Listener
	var listenErr error

	logger.Info("listen tcp")
	listener, listenErr = net.Listen("tcp", fmt.Sprintf("%s:%s", cfg.Listen.BindIP, cfg.Listen.Port))
	logger.Infof("server is listening port %s:%s", cfg.Listen.BindIP, cfg.Listen.Port)

	if listenErr != nil {
		logger.Fatal(listenErr)
	}

	fileServer := http.FileServer(http.Dir("./../../public"))
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", fileServer))

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders: []string{
			"*",
		},
	})
	handler := c.Handler(router)

	server := &http.Server{
		Handler:      handler,
		WriteTimeout: 5000 * time.Second,
		ReadTimeout:  5000 * time.Second,
	}

	logger.Fatal(server.Serve(listener))

}
