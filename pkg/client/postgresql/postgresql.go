package postgresql

import (
	"context"
	"fmt"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"factory/internal/config"
	repeatable "factory/pkg/utils"
	"net"
	"time"
)

type Client interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Begin(ctx context.Context) (pgx.Tx, error)
}

func NewClient(ctx context.Context, maxAttempts int, sc config.StorageConfig) (pool *pgxpool.Pool, err error) {
	dsn := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", sc.Username, sc.Password, sc.Host, sc.Port, sc.Database)
	err = repeatable.DoWithTries(func() error {
		//ctxx, cancel := context.WithTimeout(ctx, 5*time.Second)
		//defer cancel()
		// xxx := context.Background()
		cfg, err := pgxpool.ParseConfig(dsn)

		if err != nil {
			fmt.Println("failed to parse pg config: %w", err)
			fmt.Println("error connection database")
			// return
		}

		cfg.MaxConns = int32(sc.PgPoolMaxConn)
		//cfg.MinConns = int32(5)
		cfg.HealthCheckPeriod = 2 * time.Second
		cfg.MaxConnLifetime = 24 * time.Hour

		cfg.MaxConnIdleTime = 2 * time.Second

		cfg.ConnConfig.ConnectTimeout = 1 * time.Second
		cfg.ConnConfig.DialFunc = (&net.Dialer{
			KeepAlive: cfg.HealthCheckPeriod,
			Timeout:   cfg.ConnConfig.ConnectTimeout,
		}).DialContext
		pool, err = pgxpool.ConnectConfig(ctx, cfg)

		if err != nil {
			return err
		}

		return nil
	}, maxAttempts, 5*time.Second)

	if err != nil {
		fmt.Println(err)
		fmt.Println("error do with tries postgresql")
	}

	// Don't remove for redis
	// TODO .....
	/////////////////////// START REDIS /////////////////////////////////
	//
	//redisClient := redis.NewClient(&redis.Options{
	//	Addr:       "localhost:6379",
	//	Password:   "",
	//	DB:         0,
	//	MaxRetries: 2,
	//	PoolSize:   800,
	//	// MinIdleConns: 10,
	//})
	//
	//// FOR REDIS TEST
	//err = redisClient.Set("key", "string redis data", 0).Err()
	//if err != nil {
	//	fmt.Println("redis error -> //////////////////////////////////////////////////")
	//	panic(err)
	//}
	//
	//val, err := redisClient.Get("key").Result()
	//if err != nil {
	//	fmt.Println("redis error -> //////////////////////////////////////////////////")
	//	panic(err)
	//}
	//fmt.Println("key", val)
	//
	//// END REDIS TEST
	//
	/////////////////////// END REDIS /////////////////////////////////

	return pool, nil
}
