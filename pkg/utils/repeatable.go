package repeatable

import (
	"factory/internal/config"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"time"
)

func DoWithTries(fn func() error, attemtps int, delay time.Duration) (err error) {
	for attemtps > 0 {
		if err = fn(); err != nil {
			time.Sleep(delay)
			attemtps--

			continue
		}

		return nil
	}

	return
}

var (
	PublicFilePath = "./../../public/images"
)

func CrateDir() error {
	return os.MkdirAll(PublicFilePath, os.ModePerm)
}

func CreateFile(appName, folderName string, file multipart.File) (string, error) {
	cfg := config.GetConfig()
	appDirPath := cfg.PublicFilePath + "/" + folderName

	err := os.MkdirAll(appDirPath, os.ModePerm)
	if err != nil {
		return "str", err
	}

	pathApp := fmt.Sprintf("%s/%s", appDirPath, appName)
	dstApp, err := os.Create(pathApp)
	if err != nil {
		return pathApp, err
	}
	_, err = io.Copy(dstApp, file)
	if err != nil {
		return pathApp, err
	}

	if err != nil {
		return pathApp, err
	}

	pathApp = "/public/" + folderName + "/" + appName
	return pathApp, err
}
