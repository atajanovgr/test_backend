package appresult

import (
	"errors"
	"factory/internal/config"
	"fmt"
	"net/http"

	"github.com/golang-jwt/jwt"
)

type appHandler func(w http.ResponseWriter, r *http.Request) error

func Middleware(h appHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var appErr *AppError
		uuid := r.Header.Get("uuid")
		r.Header.Add("uuid", uuid)
		err := h(w, r)
		if err != nil {
			if errors.As(err, &appErr) {
				if errors.Is(err, ErrNotFound) {
					w.WriteHeader(http.StatusNotFound)
					w.Write(ErrNotFound.Marshal())
					return
				}
				//else if errors.Is(err, ErrNotFound) {
				//
				//}

				err = err.(*AppError)
				w.WriteHeader(http.StatusBadRequest)
				w.Write(appErr.Marshal())
				return
			}

			w.WriteHeader(http.StatusTeapot)
			w.Write(systemError(err).Marshal())
			return
		}

	}
}

func MidlTokenChkEmail(h http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		//var m repository
		token := r.Header.Get("authorization")

		cfg := config.GetConfig()
		claims, err := TokenClaims(token, cfg.JwtKey)
		if err != nil || fmt.Sprint(claims["email"]) == "" {
			w.WriteHeader(http.StatusNotAcceptable)
			w.Write(ErrNotAcceptable.Marshal())
			return
		}
		//var uuid string
		//
		//err = m.client.QueryRow(context.Background(), `select uuid from users where uuid = $1 ;`, fmt.Sprint(claims["uuid"])).Scan(&uuid)
		//if err != nil {
		//	w.WriteHeader(http.StatusNotFound)
		//	w.Write(ErrNotFound.Marshal())
		//	return
		//}

		r.Header.Add("email", fmt.Sprint(claims["email"]))

		h(w, r)
		return

	}

}

func TokenClaims(token, secretKey string) (jwt.MapClaims, error) {
	decoded, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secretKey), nil
	})

	if err != nil {
		fmt.Println("err", err)
		return nil, ErrMissingParam
	}

	claims, ok := decoded.Claims.(jwt.MapClaims)

	if !ok {
		// TODO tokenin omrini test etmeli
		return nil, ErrInternalServer
	}

	return claims, nil
}

func HeaderContentTypeJson() (string, string) {
	return "Content-Type", "application/json"
}
func AccessControlAllow() (string, string) {
	return "Access-Control-Allow-Origin", "*"
}

func MiddTokenChkSupAdmin(h http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		token := r.Header.Get("auth")
		cfg := config.GetConfig()
		claims, err := TokenClaims(token, cfg.JwtKeySupAdmin)

		if err != nil || fmt.Sprint(claims["uuid"]) == "" {
			fmt.Println(err)
			w.WriteHeader(http.StatusNotAcceptable)
			w.Write(ErrNotAcceptable.Marshal())
			return
		}

		r.Header.Add("UUID", fmt.Sprint(claims["uuid"]))

		h(w, r)
		return

	}

}
