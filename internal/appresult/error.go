package appresult

import "encoding/json"

var (
	ErrMissingParam           = NewAppError(nil, "missing param", "SE-00001")
	ErrNotFound               = NewAppError(nil, "not found", "SE-00003")
	ErrInternalServer         = NewAppError(nil, "internal server error", "SE-00004")
	ErrCountryNotFound        = NewAppError(nil, "country not found", "SE-00005")
	ErrSendingSmsLimitFull    = NewAppError(nil, "sms sending limit full, try after one hour", "SE-00006")
	ErrNotAcceptable          = NewAppError(nil, "Not acceptable", "SE-00007")
	ErrCheckPasswordLimitFull = NewAppError(nil, "ErrCheckPasswordLimitFull", "SE-00008")
	ErrUpdatePassword         = NewAppError(nil, "missing password", "SE-00010")
	ErrCheckEmail             = NewAppError(nil, "Please enter a valid email address", "SE-00011")

	//bank error code get Order Status Response Codes

	OrderBankErr0 = NewAppError(nil, "Заказ зарегистрирован, но не оплачен", "SE-00012")
	OrderBankErr1 = NewAppError(nil, "Предавторизованная сумма захолдирована (для двухстадийных платежей)", "SE-00013")
	OrderBankErr2 = NewAppError(nil, "Проведена полная авторизация суммы заказа", "SE-00014")
	OrderBankErr3 = NewAppError(nil, "Авторизация отменена", "SE-00015")
	OrderBankErr4 = NewAppError(nil, "По транзакции была проведена операция возврата", "SE-00016")
	OrderBankErr5 = NewAppError(nil, "Инициирована авторизация через ACS банка-эмитента", "SE-00017")
	OrderBankErr6 = NewAppError(nil, "Авторизация отклонена", "SE-00018")

	// get Response Error Code Options

	OptionsBankErr0 = NewAppError(nil, "Обработка запроса прошла без системных ошибок", "SE-00019")
	OptionsBankErr1 = NewAppError(nil, "Неверный номер заказа)", "SE-00020")
	OptionsBankErr2 = NewAppError(nil, "Заказ с таким номером уже обработан", "SE-00021")
	OptionsBankErr3 = NewAppError(nil, "Неизвестная валюта", "SE-00022")
	OptionsBankErr4 = NewAppError(nil, "Отсутствует сумма / Номер заказа не может быть пуст / URL возврата не может быть пуст", "SE-00023")
	OptionsBankErr5 = NewAppError(nil, "Неверно указано значение одного из параметров / Неверная сумма / Пользователь должен сменить свой пароль", "SE-00024")
	OptionsBankErr7 = NewAppError(nil, "Системная ошибка", "SE-00025")
)

type AppError struct {
	Status  bool   `json:"status"`
	Err     error  `json:"-"`
	Message string `json:"message,omitempty"`
	Code    string `json:"code,omitempty"`
}

func (e *AppError) Error() string {
	return e.Message
}

func (e *AppError) Unwrap() error { return e.Err }

func (e *AppError) Marshal() []byte {
	marshal, err := json.Marshal(e)
	if err != nil {
		return nil
	}
	return marshal
}

func NewAppError(err error, message, code string) *AppError {
	return &AppError{
		Status:  false,
		Err:     err,
		Message: message,
		Code:    code,
	}
}

func systemError(err error) *AppError {
	return NewAppError(err, "internal system error", "SE-000")
}
