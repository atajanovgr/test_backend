package setting

import "context"

type Repository interface {
	AddData(ctx context.Context, req SettingDTO) error
	UpdateData(ctx context.Context, req SettingDTO) error
	GetAll(ctx context.Context) ([]SettingDTO, error)
	Get(ctx context.Context, uuid string) (SettingDTO, error)

	DeleteData(ctx context.Context, req ReqDTO) error
}
