package db

import (
	"context"
	"factory/internal/admin/setting"
	"factory/pkg/client/postgresql"
	"factory/pkg/logging"
	"fmt"

	_ "github.com/lib/pq"
)

type repository struct {
	client postgresql.Client
	logger *logging.Logger
}

func NewRepository(client postgresql.Client, logger *logging.Logger) setting.Repository {
	return &repository{
		client: client,
		logger: logger,
	}
}

func (r *repository) GetAll(ctx context.Context) ([]setting.SettingDTO, error) {
	var (
		dt []setting.SettingDTO
	)

	qC := `select
				uuid, name, rate
			 from setting
			order by created_at desc;`
	rows, err := r.client.Query(ctx, qC)

	defer rows.Close()

	if err != nil {
		return dt, err
	}

	for rows.Next() {

		var db setting.SettingDTO
		err := rows.Scan(
			&db.UUID, &db.Name, &db.Rate,
		)

		if err != nil {
			r.logger.Error("error into rows.Next()", err)
			continue
		}

		dt = append(dt, db)

	}

	return dt, nil

}

func (r *repository) UpdateData(ctx context.Context, req setting.SettingDTO) error {

	qC := `update setting set name = $1, rate = $2 where uuid = $3;
;`
	err := r.client.QueryRow(ctx, qC, req.Name, req.Rate, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		return err
	}
	return nil
}

func (r *repository) AddData(ctx context.Context, req setting.SettingDTO) error {

	var uuid string
	qC := `INSERT INTO setting (
                       name, rate )
			VALUES ( $1, $2 ) returning uuid;
;`
	err := r.client.QueryRow(ctx, qC, req.Name, req.Rate).Scan(&uuid)

	if err != nil && err.Error() != "no rows in result set" {
		return err
	}
	return nil
}

func (r *repository) Get(ctx context.Context, uuid string) (setting.SettingDTO, error) {

	var (
		n setting.SettingDTO
	)

	q := ` select
				uuid, name, rate
 				from setting
 			where uuid = $1 ; `

	err := r.client.QueryRow(ctx, q, uuid).Scan(
		&n.UUID, &n.Name, &n.Rate,
	)
	if err != nil {
		return n, err
	}

	return n, nil
}

func (r *repository) DeleteData(ctx context.Context, req setting.ReqDTO) error {
	var (
		q string
	)

	q = ` delete from setting where uuid = $1 ;`
	err := r.client.QueryRow(ctx, q, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		fmt.Println("delete data error ::: ", err)
		return err
	}

	return nil
}
