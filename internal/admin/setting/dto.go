package setting

type FactoryDTO struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ReqDTO struct {
	UUID string `json:"uuid"`
}

type SettingDTO struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
	Rate string `json:"rate"`
}
