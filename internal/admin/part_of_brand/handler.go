package part_of_brand

import (
	"context"
	"encoding/json"
	"factory/internal/appresult"
	"factory/internal/handlers"
	"factory/pkg/logging"
	"fmt"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx"
)

const (
	getAllURL = "/get-all"
	getURL    = "/get"
	createURL = "/create"
	deleteURL = "/delete"
)

type handler struct {
	logger     *logging.Logger
	repository Repository
}

func NewHandler(logger *logging.Logger, repository Repository) handlers.Handler {
	return &handler{
		logger:     logger,
		repository: repository,
	}
}

// appresult.Middleware(h.TokenService),

func (h *handler) Register(router *mux.Router) {

	router.HandleFunc(getAllURL, appresult.Middleware(h.GetAll)).Methods("POST")
	router.HandleFunc(getURL, appresult.Middleware(h.Get)).Methods("POST")

	router.HandleFunc(createURL, appresult.Middleware(h.Create)).Methods("POST")
	router.HandleFunc(deleteURL, appresult.Middleware(h.Delete)).Methods("POST")

}

// =================================================================================

func (h *handler) GetAll(w http.ResponseWriter, r *http.Request) error {
	uuid := r.Header.Get("uuid")
	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return appresult.ErrMissingParam
	}

	reqDTO := PaginationDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return appresult.ErrMissingParam
	}

	resDTO, err := h.repository.GetAll(context.TODO(), uuid, reqDTO)
	if err != nil && err.Error() == pgx.ErrNoRows.Error() {
		return err
	}

	successResult := appresult.Success
	successResult.Data = resDTO

	if err != nil {
		return err
	}

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil

}

func (h *handler) Get(w http.ResponseWriter, r *http.Request) error {
	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return appresult.ErrMissingParam
	}

	reqDTO := ReqDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return appresult.ErrMissingParam
	}
	resDTO, err := h.repository.Get(context.TODO(), reqDTO.UUID)

	if err != nil && err.Error() == pgx.ErrNoRows.Error() {
		return err
	}

	successResult := appresult.Success
	successResult.Data = resDTO

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil

}

func (h *handler) Create(w http.ResponseWriter, r *http.Request) error {

	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return errBody
	}

	reqDTO := PartBrandDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return err
	}

	fmt.Println("errrrrrrrrrrrrrrrrrr-------------", err)

	if reqDTO.UUID != "" {
		err = h.repository.UpdateData(context.Background(), reqDTO)
	} else {
		err = h.repository.AddData(context.Background(), reqDTO)
	}

	if err != nil {
		return err
	}

	successResult := appresult.Success
	successResult.Data = ""
	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil
}

func (h *handler) Delete(w http.ResponseWriter, r *http.Request) error {

	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return errBody
	}

	reqDTO := ReqDTO{}
	err := json.Unmarshal(body, &reqDTO)

	err = h.repository.DeleteData(context.Background(), reqDTO)
	if err != nil {
		return err
	}

	successResult := appresult.Success
	successResult.Data = ""
	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return appresult.ErrInternalServer
	}

	return nil

}
