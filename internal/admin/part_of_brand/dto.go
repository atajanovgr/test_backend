package part_of_brand

type ReqDTO struct {
	UUID string `json:"uuid"`
}

type PartBrandDTO struct {
	UUID       string      `json:"uuid"`
	BrId       string      `json:"brId"`
	BrandName  string      `json:"brName"`
	Price      int         `json:"price"`
	Content    string      `json:"content"`
	Collection interface{} `json:"collection"`
}

type PaginationDTO struct {
	Limit  int    `json:"limit"`
	Page   int    `json:"page"`
	Search string `json:"search"`
}

type DataDTO struct {
	PartBrandDTO []PartBrandDTO `json:"data"`
	Count        int            `json:"count"`
}
