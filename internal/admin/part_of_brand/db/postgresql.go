package db

import (
	"context"
	"factory/internal/admin/part_of_brand"
	"factory/pkg/client/postgresql"
	"factory/pkg/logging"
	"fmt"

	_ "github.com/lib/pq"
)

type repository struct {
	client postgresql.Client
	logger *logging.Logger
}

func NewRepository(client postgresql.Client, logger *logging.Logger) part_of_brand.Repository {
	return &repository{
		client: client,
		logger: logger,
	}
}

func (r *repository) GetAll(ctx context.Context, uuid string, req part_of_brand.PaginationDTO) (part_of_brand.DataDTO, error) {
	var (
		dt   []part_of_brand.PartBrandDTO
		data part_of_brand.DataDTO
	)

	q := `select count(*) from part_brand;`
	r.client.QueryRow(ctx, q).Scan(&data.Count)

	qC := `select
			 pb.uuid, bs.name,
			 price, content,
			 collection
				  from  part_brand pb 
		 	left join brends bs on bs.uuid = pb.br_id
			where bs.fc_id = $4 and bs.name ILIKE TRIM (LOWER( $1 || '%'))
			order by pb.created_at desc limit $2 offset $3 ;`
	rows, err := r.client.Query(ctx, qC, req.Search, req.Limit, (req.Page-1)*req.Limit, uuid)

	defer rows.Close()

	if err != nil {
		return data, err
	}

	for rows.Next() {

		var db part_of_brand.PartBrandDTO
		err := rows.Scan(
			&db.UUID, &db.BrandName,
			&db.Price, &db.Content,
			&db.Collection,
		)

		if err != nil {
			r.logger.Error("error into rows.Next()", err)
			continue
		}

		dt = append(dt, db)

	}
	data.PartBrandDTO = dt

	return data, nil

}

func (r *repository) UpdateData(ctx context.Context, req part_of_brand.PartBrandDTO) error {

	qC := `update part_brand set price = $1, content = $2, collection = $3, br_id = $4 where uuid = $5;
;`
	err := r.client.QueryRow(ctx, qC, req.Price, req.Content, req.Collection, req.BrId, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		return err
	}
	return nil
}

func (r *repository) AddData(ctx context.Context, req part_of_brand.PartBrandDTO) error {

	var uuid string
	qC := `INSERT INTO part_brand (
                       br_id, price, content, collection )
			VALUES ( $1, $2, $3, $4 ) returning uuid;
;`
	err := r.client.QueryRow(ctx, qC, req.BrId, req.Price, req.Content, req.Collection).Scan(&uuid)

	if err != nil && err.Error() != "no rows in result set" {
		fmt.Println("part of brand", req, err)
		return err
	}
	return nil
}

func (r *repository) Get(ctx context.Context, uuid string) (part_of_brand.PartBrandDTO, error) {

	var (
		n part_of_brand.PartBrandDTO
	)

	q := ` select
				pb.uuid, pb.br_id,
				price, content,
				collection
 					from  part_brand pb 
 			where pb.uuid = $1 ; `

	err := r.client.QueryRow(ctx, q, uuid).Scan(
		&n.UUID, &n.BrId,
		&n.Price, &n.Content,
		&n.Collection,
	)
	if err != nil {
		return n, err
	}

	return n, nil
}

func (r *repository) DeleteData(ctx context.Context, req part_of_brand.ReqDTO) error {
	var (
		q string
	)

	q = ` delete from part_brand where uuid = $1 ;`
	err := r.client.QueryRow(ctx, q, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		fmt.Println("delete data error ::: ", err)
		return err
	}

	return nil
}
