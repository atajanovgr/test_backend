package part_of_brand

import "context"

type Repository interface {
	AddData(ctx context.Context, req PartBrandDTO) error
	UpdateData(ctx context.Context, req PartBrandDTO) error
	GetAll(ctx context.Context, uuid string, req PaginationDTO) (DataDTO, error)
	Get(ctx context.Context, uuid string) (PartBrandDTO, error)
	// /
	DeleteData(ctx context.Context, req ReqDTO) error
}
