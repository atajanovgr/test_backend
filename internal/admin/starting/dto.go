package starting

type loginDTO struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type ResultloginDTO struct {
	UUID     string `json:"uuid"`
	Password string `json:"password"`
}

type ResultTokenDTO struct {
	Token string `json:"token"`
}

type FactoryDTO struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ReqDTO struct {
	UUID string `json:"uuid"`
}

type BrendDTO struct {
	UUID string `json:"uuid"`
	FcID string `json:"fcId"`
	Name string `json:"name"`
	Path string `json:"path"`
}

type PaginationDTO struct {
	Limit  int    `json:"limit"`
	Page   int    `json:"page"`
	Search string `json:"search"`
}

type DataDTO struct {
	BrendDTO []BrendDTO `json:"data"`
	Count    int        `json:"count"`
}
