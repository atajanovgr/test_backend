package db

import (
	"context"
	"factory/internal/admin/starting"
	"factory/pkg/client/postgresql"
	"factory/pkg/logging"
	"fmt"

	_ "github.com/lib/pq"
)

type repository struct {
	client postgresql.Client
	logger *logging.Logger
}

func NewRepository(client postgresql.Client, logger *logging.Logger) starting.Repository {
	return &repository{
		client: client,
		logger: logger,
	}
}

func (r *repository) LoginAdmin(ctx context.Context, login string) (starting.ResultloginDTO, error) {
	q := `	
		select 
			uuid, password 
		from users
		where login = $1;`

	var result starting.ResultloginDTO

	err := r.client.QueryRow(ctx, q, login).Scan(&result.UUID, &result.Password)
	if err != nil {
		return result, err
	}
	return result, nil
}

func (r *repository) GetFactory(ctx context.Context) ([]starting.FactoryDTO, error) {

	var (
		ns []starting.FactoryDTO
	)

	q := ` select uuid, name
 					from factories fc
 			order by fc.created_at desc
 ; `

	rows, err := r.client.Query(ctx, q)
	defer rows.Close()

	if err != nil {
		fmt.Println("art data error ::: ", err)
		return ns, nil
	}

	for rows.Next() {
		var n starting.FactoryDTO
		errN := rows.Scan(
			&n.UUID, &n.Name,
		)
		if errN != nil {
			r.logger.Error("errN :::", errN)
			continue
		}

		ns = append(ns, n)
	}

	return ns, nil
}

func (r *repository) GetBrends(ctx context.Context, uuid string, req starting.PaginationDTO) (starting.DataDTO, error) {
	var (
		dt   []starting.BrendDTO
		data starting.DataDTO
	)

	q := `select count(*) from brends where fc_id = $1;`
	r.client.QueryRow(ctx, q, uuid).Scan(&data.Count)

	qC := `select 
				uuid, name, path
			 from brends
			where fc_id = $1 and 
			name ILIKE TRIM (LOWER( $4 || '%'))
			order by created_at desc limit $2 offset $3 ;`
	rows, err := r.client.Query(ctx, qC, uuid, req.Limit, (req.Page-1)*req.Limit, req.Search)

	defer rows.Close()

	if err != nil {
		return data, err
	}

	for rows.Next() {

		var db starting.BrendDTO
		err := rows.Scan(
			&db.UUID, &db.Name, &db.Path,
		)

		if err != nil {
			r.logger.Error("error into rows.Next()", err)
			continue
		}

		dt = append(dt, db)

	}
	data.BrendDTO = dt

	return data, nil

}

func (r *repository) UpdateBrendData(ctx context.Context, req starting.BrendDTO) error {

	qC := `update brends set name = $1, path = $2 where uuid = $3;
;`
	err := r.client.QueryRow(ctx, qC, req.Name, req.Path, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		return err
	}
	return nil
}

func (r *repository) AddBrendData(ctx context.Context, req starting.BrendDTO) error {

	var uuid string
	qC := `INSERT INTO brends (
                       fc_id, name, path )
			VALUES ( $1, $2,$3 ) returning uuid;
;`
	err := r.client.QueryRow(ctx, qC, req.FcID, req.Name, req.Path).Scan(&uuid)

	if err != nil && err.Error() != "no rows in result set" {
		return err
	}
	return nil
}

func (r *repository) GetBrend(ctx context.Context, uuid string) (starting.BrendDTO, error) {

	var (
		n starting.BrendDTO
	)

	q := ` select 
     			uuid, name , path
 					from  brends
 			where uuid = $1 ; `

	err := r.client.QueryRow(ctx, q, uuid).Scan(
		&n.UUID, &n.Name, &n.Path,
	)
	if err != nil {
		return n, err
	}

	return n, nil
}

func (r *repository) DeleteData(ctx context.Context, req starting.ReqDTO) error {
	var (
		q string
	)

	q = ` delete from brends where uuid = $1 ;`
	err := r.client.QueryRow(ctx, q, req.UUID).Scan()

	if err != nil && err.Error() != "no rows in result set" {
		fmt.Println("delete data error ::: ", err)
		return err
	}

	return nil
}
