package starting

import "context"

type Repository interface {
	LoginAdmin(ctx context.Context, login string) (ResultloginDTO, error)
	AddBrendData(ctx context.Context, req BrendDTO) error
	UpdateBrendData(ctx context.Context, req BrendDTO) error
	GetBrends(ctx context.Context, uuid string, req PaginationDTO) (DataDTO, error)
	GetBrend(ctx context.Context, uuid string) (BrendDTO, error)

	DeleteData(ctx context.Context, req ReqDTO) error
	GetFactory(ctx context.Context) ([]FactoryDTO, error)
}
