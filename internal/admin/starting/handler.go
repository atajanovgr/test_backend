package starting

import (
	"context"
	"encoding/json"
	"factory/internal/appresult"
	"factory/internal/config"
	"factory/internal/handlers"
	"factory/pkg/logging"
	rp "factory/pkg/utils"
	"fmt"
	"io"
	"net/http"
	"path/filepath"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx"
	"golang.org/x/crypto/bcrypt"
)

const (
	loginURL      = "/login"
	getFactoryURL = "/get-factory"

	getBrendsURL   = "/get-brends"
	getBrendURL    = "/get-brend"
	createBrendURL = "/create-brend"
	deleteURL      = "/delete-brend"

	fileUploadURL = "/file"
)

type handler struct {
	logger     *logging.Logger
	repository Repository
}

func NewHandler(logger *logging.Logger, repository Repository) handlers.Handler {
	return &handler{
		logger:     logger,
		repository: repository,
	}
}

// appresult.Middleware(h.TokenService),

func (h *handler) Register(router *mux.Router) {
	router.HandleFunc(loginURL, appresult.Middleware(h.Login)).Methods("POST")
	router.HandleFunc(getFactoryURL, appresult.Middleware(h.GetFactory)).Methods("GET")

	router.HandleFunc(getBrendsURL, appresult.Middleware(h.GetBrends)).Methods("POST")
	router.HandleFunc(getBrendURL, appresult.Middleware(h.GetBrend)).Methods("POST")

	router.HandleFunc(createBrendURL, appresult.Middleware(h.CreateBrend)).Methods("POST")
	router.HandleFunc(deleteURL, appresult.Middleware(h.Delete)).Methods("POST")
	router.HandleFunc(fileUploadURL, appresult.Middleware(h.FileUpload)).Methods("POST")

}

// =================================================================================

func (h *handler) Login(w http.ResponseWriter, r *http.Request) error {

	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return errBody
	}

	LoginDTO := loginDTO{}
	errData := json.Unmarshal(body, &LoginDTO)
	if errData != nil {
		return errData
	}

	resultloginDTO, err := h.repository.LoginAdmin(context.TODO(), LoginDTO.Login)
	if err != nil {
		return err
	}
	password := []byte(fmt.Sprint(LoginDTO.Password))
	passwordHash := []byte(fmt.Sprint(resultloginDTO.Password))
	errCompareHash := bcrypt.CompareHashAndPassword(passwordHash, password)
	if errCompareHash != nil {
		return err
	}

	var tokenDTO string

	atClaims := jwt.MapClaims{}
	atClaims["uuid"] = resultloginDTO.UUID
	atClaims["exp"] = time.Now().Add(time.Minute * 60 * 24 * 360).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)

	cfg := config.GetConfig()
	tokenDTO, errToken := at.SignedString([]byte(cfg.JwtKeySupAdmin))
	if errToken != nil {
		return err
	}

	successResult := appresult.Success
	successResult.Data = ResultTokenDTO{
		Token: tokenDTO,
	}

	if err != nil {
		return err
	}

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil
}

func (h *handler) GetFactory(w http.ResponseWriter, r *http.Request) error {

	dt, err := h.repository.GetFactory(context.TODO())

	successResult := appresult.Success
	successResult.Data = dt

	if err != nil {
		return err
	}

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil
}

func (h *handler) GetBrends(w http.ResponseWriter, r *http.Request) error {
	uuid := r.Header.Get("uuid")

	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return appresult.ErrMissingParam
	}

	reqDTO := PaginationDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return appresult.ErrMissingParam
	}

	resDTO, err := h.repository.GetBrends(context.TODO(), uuid, reqDTO)
	if err != nil && err.Error() == pgx.ErrNoRows.Error() {
		return err
	}

	successResult := appresult.Success
	successResult.Data = resDTO

	if err != nil {
		return err
	}

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil

}

func (h *handler) GetBrend(w http.ResponseWriter, r *http.Request) error {
	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return appresult.ErrMissingParam
	}

	reqDTO := ReqDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return appresult.ErrMissingParam
	}
	resDTO, err := h.repository.GetBrend(context.TODO(), reqDTO.UUID)

	if err != nil && err.Error() == pgx.ErrNoRows.Error() {
		return err
	}

	successResult := appresult.Success
	successResult.Data = resDTO

	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil

}

func (h *handler) CreateBrend(w http.ResponseWriter, r *http.Request) error {

	uuid := r.Header.Get("uuid")
	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return errBody
	}

	reqDTO := BrendDTO{}
	err := json.Unmarshal(body, &reqDTO)

	if err != nil {
		return err
	}
	reqDTO.FcID = uuid

	if reqDTO.UUID != "" {
		err = h.repository.UpdateBrendData(context.Background(), reqDTO)
	} else {
		err = h.repository.AddBrendData(context.Background(), reqDTO)
	}

	if err != nil {
		return err
	}

	successResult := appresult.Success
	successResult.Data = ""
	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return err
	}
	return nil
}

func (h *handler) FileUpload(w http.ResponseWriter, r *http.Request) error {
	var (
		res string
		err error
	)
	file, fileHeader, _ := r.FormFile("file")

	if file != nil {

		defer file.Close()

		appName := fmt.Sprintf("%d%s", time.Now().UnixNano(), filepath.Ext(fileHeader.Filename))
		t, err := rp.CreateFile(appName, "brend", file)

		if err != nil {
			return appresult.ErrInternalServer
		}
		res = t
	}

	if err != nil {
		return appresult.ErrInternalServer
	}

	successResult := appresult.Success
	successResult.Data = res
	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return appresult.ErrInternalServer
	}

	return nil
}

func (h *handler) Delete(w http.ResponseWriter, r *http.Request) error {

	body, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		return errBody
	}

	reqDTO := ReqDTO{}
	err := json.Unmarshal(body, &reqDTO)

	err = h.repository.DeleteData(context.Background(), reqDTO)
	if err != nil {
		return err
	}

	successResult := appresult.Success
	successResult.Data = ""
	w.Header().Add(appresult.HeaderContentTypeJson())
	err = json.NewEncoder(w).Encode(successResult)
	if err != nil {
		return appresult.ErrInternalServer
	}

	return nil

}
