package handlermanager

import (
	"factory/internal/admin/part_of_brand"
	partdb "factory/internal/admin/part_of_brand/db"
	"factory/internal/admin/setting"
	settingdb "factory/internal/admin/setting/db"
	"factory/internal/admin/starting"
	startingdb "factory/internal/admin/starting/db"
	"factory/pkg/logging"

	"github.com/jackc/pgx/v4/pgxpool"

	"github.com/gorilla/mux"
)

const (
	partBrandURL = "/api/v1/part"
	startingURL  = "/api/v1/start"
	settingURL   = "/api/v1/setting"
)

func Manager(client *pgxpool.Pool, logger *logging.Logger) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	startingRouterManager := router.PathPrefix(startingURL).Subrouter()
	startingRepository := startingdb.NewRepository(client, logger)
	startingRouterHandler := starting.NewHandler(logger, startingRepository)
	startingRouterHandler.Register(startingRouterManager)

	settingRouterManager := router.PathPrefix(settingURL).Subrouter()
	settingRepository := settingdb.NewRepository(client, logger)
	settingRouterHandler := setting.NewHandler(logger, settingRepository)
	settingRouterHandler.Register(settingRouterManager)

	partRouterManager := router.PathPrefix(partBrandURL).Subrouter()
	partRepository := partdb.NewRepository(client, logger)
	partRouterHandler := part_of_brand.NewHandler(logger, partRepository)
	partRouterHandler.Register(partRouterManager)

	return router
}
