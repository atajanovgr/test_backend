CREATE DATABASE test;

\c test;

SET client_encoding TO 'UTF-8';
SHOW client_encoding;

CREATE ROLE test_user LOGIN SUPERUSER PASSWORD 'test_2023';
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


CREATE TABLE users (
       "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
       "login" character varying(250) NOT NULL DEFAULT '',
       "password" character varying(250) NOT NULL DEFAULT '',

       "updated_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
       "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL
);

--  password  zurnal2023
insert into users(
            login, password
    ) values (
    'admin',
    '$2a$12$DEt5PmRatYtw0h5IjHD13exojZsVN32b6xRacRD0jWiCU4zsLMM1u'
    );


CREATE TABLE factories (
     "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
     "name" character varying(250) NOT NULL DEFAULT '',
     "updated_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
     "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL
);

insert into factories (name) values('zawod_1');
insert into factories (name) values('zawod_2');
insert into factories (name) values('zawod_3');

CREATE TABLE brends (
     "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
     "fc_id" uuid NOT NULL ,
     "name" character varying(250) NOT NULL DEFAULT '',
     "path" character varying(250) NOT NULL DEFAULT '',
     "updated_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
     "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,

       CONSTRAINT factory_id_fk
         FOREIGN KEY ("fc_id")
             REFERENCES factories("uuid")
             ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE part_brand (
       "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
       "br_id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
       "price" SMALLINT NOT NULL DEFAULT 0,
       "content" character varying(500) NOT NULL DEFAULT '',
       "collection" json NOT NULL DEFAULT '{}'::json,

       "updated_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
       "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
       CONSTRAINT br_id_fk
              FOREIGN KEY ("br_id")
                     REFERENCES brends("uuid")
                     ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE setting (
     "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
     "name" character varying(250) NOT NULL DEFAULT '',
     "rate" character varying(250) NOT NULL DEFAULT '',
     "updated_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL,
     "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')  NOT NULL
);